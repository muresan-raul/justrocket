import React from 'react';
import './App.scss';
import { LoaderComponent } from './components/Loader.Component/Loader.Component';

const App = () => {
  const dataSource = 'http://api.mediastack.com/v1/news?access_key=17b1ea2f282ea579859f61c6e8d1df02&limit={limit}&offset={offset}';
  const itemsOnPage = 5;
  const startPage = 0;
  return (
    <div className="App">
      <LoaderComponent
        url={dataSource}
        limit={itemsOnPage}
        offset={startPage}
        height={'90rem'}
        width={'90rem'}
      />
    </div>
  );
}

export default App;
