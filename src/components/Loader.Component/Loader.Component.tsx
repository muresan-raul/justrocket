import React, { useEffect, useState, ReactNode } from 'react';

import { formatContent, getCurrentOffset, requestFeed } from './Loader.Service';
import {ERROR_LOADING_CONTENT, DEFAULT_HEIGHT, DEFAULT_OFFSET, DEFAULT_LIMIT} from './Loader.Config';
import './Loader.Component.scss'

type LoaderComponentPropsType = {
    url: string,
    offset?: number,
    limit?: number,
    height?: string,
    width?: string
}

export const LoaderComponent = (props: LoaderComponentPropsType) => {
    const {
        url,
        offset: initialOffset = DEFAULT_OFFSET,
        limit = DEFAULT_LIMIT,
        height = DEFAULT_HEIGHT,
        width = DEFAULT_LIMIT
    } = props

    const initialContent: ReactNode[] = [];

    const [offset, updateOffset] = useState(initialOffset);
    const [content, updateContent] = useState(initialContent);
    const [isLoading, setIsLoading] = useState(false);
    const [totalPages, settotalPages] = useState(-1);

    const loadContent = async () => {
        try {
            if (isLoading) return;
            if (totalPages > -1 && offset >= totalPages - 1) return;

            setIsLoading(true);
            const currentOffset = getCurrentOffset(offset, !!content)
            const rawFeed = await requestFeed(url, currentOffset, limit);
            debugger;
            if (rawFeed?.status === 200 && rawFeed.statusText === "OK") {
                const { data, pagination } = rawFeed?.data;
                if (data) {
                    const formatedContent = formatContent(data, currentOffset);
                    const newContent = [...content, ...formatedContent];
                    updateContent(newContent);
                    updateOffset(currentOffset);
                    setIsLoading(false);
                } else {
                    alert(ERROR_LOADING_CONTENT);
                }
                if (pagination) {
                    settotalPages(pagination.total)
                }
            } else {
                alert(ERROR_LOADING_CONTENT);
            }
        } catch (e) {
            alert(ERROR_LOADING_CONTENT);
        }

    }

    useEffect(() => {
        loadContent();
    }, []);

    const handleScroll = (e: any) => {
        const bottom = e.target.scrollHeight - e.target.scrollTop === e.target.clientHeight;
        if (bottom) {
            loadContent();
        }
    }
    const style = { height, width };

    return (
        <div className='LoaderComponent' style={style}>
            <div className='LoaderComponent__header'>
                <h3>Justrocket loader</h3>
            </div>
            <div
                className='LoaderComponent__body'
                onScroll={handleScroll}
            >
                <div>
                    {content}
                    <div
                        className={`LoaderComponent__loading-indicator ${isLoading && 'loading'}`}
                    >
                        <div>Loading...</div>
                    </div>

                </div>

            </div>

            <div className='LoaderComponent__footer'>
                <div className='LoaderComponent__interface'>
                    <div className='LoaderComponent__pageNr'>page: {offset}</div>
                    <div className='LoaderComponent__itemsOnPage'>items per page: {limit}</div>
                    <div className='LoaderComponent__totalPages'>total pages: {totalPages}</div>
                    <button type="button" onClick={loadContent}>{'NEXT >'}</button>
                </div>
            </div>
        </div>
    )
};
// kszkmadoqehcqncfly@mrvpm.net
// abc123
// 17b1ea2f282ea579859f61c6e8d1df02
// https://mediastack.com/quickstart