export const ERROR_LOADING_CONTENT = 'Error loading the content!';
export const DEFAULT_HEIGHT = '90rem';
export const DEFAULT_WIDTH = '90rem';
export const DEFAULT_OFFSET = 0;
export const DEFAULT_LIMIT = 0;