import React, { ReactNode } from "react";
import { AxiosResponse } from "axios";
import moment from 'moment'

import RestHelper from "../../api/RestHelper";

enum formatEndpointParams { offset = 'offset', limit = 'limit'};
type formatEndpointParamsType = {
    [key in keyof typeof formatEndpointParams]: string|number
}
type formatContentDataType = {
    [key:string]: string|number|null
  }
// formats the url for the api source
const formatEndpoint = (path: string ='', params: formatEndpointParamsType): string => {
    const keys: Array<keyof formatEndpointParamsType> = Object.keys(params) as Array<keyof formatEndpointParamsType>;
    let newPath = path.trim();
    keys.forEach((item: keyof typeof params) => {
        const regex = new RegExp(`{${item}}`, 'gm');
        const substring = params[item] as formatEndpointParams;
        newPath = newPath.replace(regex, substring);
    })
    return newPath;
}

export const requestFeed = async (url: string, offset: number, limit: number): Promise<AxiosResponse<any>> => {
    const sourceUrl = formatEndpoint(url, {offset, limit});
    const data = await RestHelper.get(sourceUrl);
    return data
}


// formats the content in reactNode structure
export const formatContent = (data: formatContentDataType[], offset: number): ReactNode[] => {
    const formatedContent: ReactNode[] = [];
    data.forEach((element: formatContentDataType, i: number) => {
        const formatedArticle: ReactNode[] = [];
        const keys: Array<keyof formatContentDataType> = Object.keys(element);// as Array<keyof formatEndpointParamsType>;

        keys.forEach((item: keyof typeof element, j: number) => {
            const key = `article-section-${offset}-${i}-${j}`;
            let newContent = formatFragment(element[item], item, key);// element[item];
            formatedArticle.push(newContent);
        })
        formatedContent.push(React.createElement('div', {className: 'article', key:`article-${offset}-${i}`}, formatedArticle))

    });

    return formatedContent;
}
// formats sections of content depending on type
const formatFragment = (data: string|number|null, label: keyof formatContentDataType, key: string) => {
    const content = data !== null ? data : '';
    let element: ReactNode;
    switch (label) {
        case 'title':
            element = React.createElement('div', {className: label, key}, <h2>{content}</h2>);
          break;
        case 'url':
            if(typeof content === 'string')
            element = React.createElement('div', {className: label, key}, <a href={content}>{content}</a>);
            break;
        case 'image':
            if(typeof content === 'string')
            element = React.createElement('div', {className: label, key}, <img src={content} alt=""></img>);
            break;
        case 'published_at':
            if(typeof content === 'string'){
            const date = moment(content).format('MMMM Do YYYY')
            element = React.createElement('div', {className: label, key}, `${label}: ${date}`);
            }
            break;
        case 'author':
        case 'country':
        case 'description':
        case 'language':
        case 'source':
        case 'category':
            if(typeof content === 'string')
            element = React.createElement('div', {className: label, key}, `${label}: ${content}`);
            break;
        default:
            element = React.createElement('div', {className: label, key}, content);
      }
return element;
}
export const getCurrentOffset = (existingOffset: number, content: boolean): number => {
    if(!existingOffset && !content){return 0};
    return ++existingOffset;
}

